#!/usr/bin/bash

if [ $# -ne 1 ] || [ $1 != "MntFolderReady" ]; then
  echo -e "Ensure disk is partitioned and formatted and mounted to /mnt"
  echo -e "Also ensure if in efi mode that efi part is mounted to /mnt/efi"
  echo -e "E.g. fdisk /dev/sdX; 64MB(s1 min size) efi part. rest linux part"
  echo -e "\t mkfs.vfat -F32 -s1 /efi/part"
  echo -e "\t mkfs.ext4 /linux/part"
  echo -e "If in BIOS mode 1MB unformatted part for grub, rest for linux"
  echo -e "When ready call this script as $0 MntFolderReady"
  exit 1
fi

pacman-key --init
pacman-key --populate archlinux
pacstrap -K /mnt $(cat /packages.x86_64|sed 's/$/ /g'|tr -d '\n')
genfstab -U /mnt >> /mnt/etc/fstab
cp -r /laptop_configs /mnt/laptop_configs

echo "'arch-chroot /mnt' can now be used to install grub"
