#!/usr/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "Please run as root."
  exit 1
fi

if [ $# -ne 0 ]; then
  echo "Usage: $0"
  exit 1
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
LOCALREPO_DIR="$SCRIPT_DIR/airootfs/localrepo"
WORK_DIR="/archisotmpdir"
LOCALREPO_SYMLINK="/mkarchiso-localrepo"

ln -sf $LOCALREPO_DIR $LOCALREPO_SYMLINK
mkdir $LOCALREPO_DIR
mkdir $WORK_DIR
# Download all packages to a separate folder, dbpath used to ignore system db so dependencies are also installed
pacman -Syw --cachedir $LOCALREPO_DIR --dbpath $WORK_DIR $(cat $SCRIPT_DIR/packages.x86_64|sed 's/$/ /g'|tr -d '\n')
rm -rf $WORK_DIR
repo-add $LOCALREPO_DIR/localrepo.db.tar.gz $LOCALREPO_DIR/*.pkg.tar.zst
mkdir $WORK_DIR
mkarchiso -v -w $WORK_DIR -o $SCRIPT_DIR $SCRIPT_DIR
rm -rf $WORK_DIR
rm $LOCALREPO_SYMLINK
